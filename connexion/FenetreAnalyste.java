import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Side;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.layout.Pane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class FenetreAnalyste extends BorderPane {
    private Button bouton;
    public FenetreAnalyste(Button bouton) {
        super();
        this.bouton = bouton;
        BorderPane top = new BorderPane();
        Text textGauche = new Text("Allo 45 - Module Analyste");
        textGauche.setFont(Font.font("Arial", FontWeight.BOLD, 32));
        top.setLeft(textGauche);
        top.setRight(this.bouton);
        top.setStyle("-fx-background-color:#4682B4");
        top.setPadding(new Insets(20));
        VBox center = new VBox();
        Text text = new Text("Analyse du Sondage sur les habitudes alimentaires");
        text.setFont(Font.font("Arial", 20));
        center.setSpacing(10);
        ComboBox<String> choix = new ComboBox<>();
        choix.getItems().addAll("Pie","Graphique circulaire");
        choix.setValue("Pie");
        PieChart chart = new PieChart();
        chart.setTitle("Que lisez-vous au petit déjeuner ?");
        chart.getData().setAll( 
        new PieChart.Data("Le journal", 21),
        new PieChart.Data("Un livre", 3),
        new PieChart.Data("Le courier", 7),
        new PieChart.Data ("La boîte de céréales", 75));
        chart.setLegendSide(Side.LEFT);
        HBox buttons = new HBox();
        buttons.setPadding(new Insets(20));
        buttons.setSpacing(10);
        Button questionPrecedente = new Button("QuestionPrecedente", new ImageView("./graphics/back.png"));
        Button questionSuivante = new Button("QuestionSuivante", new ImageView("./graphics/next.png"));
        buttons.getChildren().addAll(questionPrecedente, questionSuivante);
        center.getChildren().addAll(text, choix, chart, buttons);
        TilePane images = new TilePane();
        ImageView img1 = new ImageView("./graphics/chart_1.png");
        ImageView img2 = new ImageView("./graphics/chart_2.png");
        ImageView img3 = new ImageView("./graphics/chart_3.png");
        ImageView img4 = new ImageView("./graphics/chart_4.png");
        ImageView img5 = new ImageView("./graphics/chart_5.png");
        ImageView img6 = new ImageView("./graphics/chart_6.png");
        ImageView img7 = new ImageView("./graphics/chart_7.png");
        images.getChildren().addAll(img1, img2, img3, img4, img5, img6, img7);
        images.setStyle("-fx-background-color:#77b5fe");
        this.setTop(top);
        this.setCenter(center);
        this.setRight(images);
    }
}
