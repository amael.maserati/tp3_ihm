import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.scene.image.ImageView;

 
public class AppliPlusieursFenetres extends Application {
    
    private Button btnDeconnexion;
    private Button btnConnexion;
    private Scene scene;
 
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(AppliPlusieursFenetres.class, args);
    }
    
    @Override
    public void init(){
        this.btnDeconnexion = new Button("Deconnexion", new ImageView("./graphics/user.png"));       
        this.btnConnexion = new Button("Connexion");

        ControleBouton controleur = new ControleBouton(this);        
        this.btnDeconnexion.setOnAction(controleur);
        this.btnConnexion.setOnAction(controleur);
        
    }
    
    @Override
    public void start(Stage stage) {
        Pane root = new FenetreConnexion(this.btnConnexion);
        this.scene = new Scene(root, 900, 700);
        stage.setScene(scene);
        stage.setTitle("Allo 45");
        stage.show();
    }
    public void afficheFenetreConnexion() {
        Pane root = new FenetreConnexion(this.btnConnexion);
        this.scene.setRoot(root);
    }
    public void afficheFenetreAnalyste(){
        Pane root = new FenetreAnalyste(this.btnDeconnexion);
        this.scene.setRoot(root);
    }
}
