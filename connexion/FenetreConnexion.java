import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class FenetreConnexion extends GridPane {
    private TextField tfid;
    private TextField tfmdp;
    private Button connexion;
    public FenetreConnexion(Button bouton) {
        super();
        this.tfid = new TextField();
        this.tfmdp = new TextField();
        this.connexion = bouton;
        this.add(new Label("Entrez votre identifiant et votre mot de passe"), 0, 0, 4, 1);
        this.add(new Label("Identifiant"), 0, 2);
        this.add(this.tfid, 1, 2, 2, 1);
        this.add(new Label("Mot de passe"), 0, 3);
        this.add(tfmdp, 1, 3, 2, 1);
        this.add(this.connexion, 2, 4);
        this.setHgap(10);
        this.setVgap(12);
        this.setPadding(new Insets(10));
        GridPane.setHalignment(this.connexion, HPos.RIGHT);
    }
}
